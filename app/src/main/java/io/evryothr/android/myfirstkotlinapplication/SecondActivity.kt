package io.evryothr.android.myfirstkotlinapplication

import kotlinx.android.synthetic.main.activity_second.textview_random
import kotlinx.android.synthetic.main.activity_second.textview_label
import android.support.v7.app.AppCompatActivity
import android.os.Bundle
import java.util.*

class SecondActivity : AppCompatActivity() {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_second)
        showRandomNumber()
    }

    companion object {
        const val TOTAL_COUNT = "total_count"
    }

    fun showRandomNumber() {
        val count = intent.getIntExtra(TOTAL_COUNT, 0)

        val random = Random()

        textview_random.text = if (count > 0) {
            random.nextInt(count + 1).toString()
        } else {
            count.toString()
        }

        textview_label.text = getString(R.string.random_heading, count)
    }
}

