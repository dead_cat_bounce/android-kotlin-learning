package io.evryothr.android.myfirstkotlinapplication

import android.content.Intent
import kotlinx.android.synthetic.main.activity_main.textView
import android.support.v7.app.AppCompatActivity
import android.os.Bundle
import android.view.View
import android.widget.Toast
import io.evryothr.android.myfirstkotlinapplication.R.string.count

class MainActivity : AppCompatActivity() {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)
    }

    fun toastMe(view: View): Unit {
        // val myToast = Toast.makeText(this, message, duration)
        val myToast = Toast.makeText(this, "Hola Toasties!", Toast.LENGTH_SHORT)
        myToast.show()
    }

    fun randomMe(view: View): Unit {
        val randomIntent = Intent(this, SecondActivity::class.java)
        val countString = textView.text.toString()
        val count: Int = Integer.parseInt(countString)

        randomIntent.putExtra(SecondActivity.TOTAL_COUNT, count)

        // Start the new activity..
        startActivity(randomIntent)
    }

    fun countMe(view: View): Unit {
        // Get the text view:
        // Eliminate this by using kotlin.android.syn... import as ^^
//        val showCountTextView = findViewById(R.id.textView) as TextView

        // Get the value  of the text view:
//        val countString = showCountTextView.text.toString()
        val countString =textView.text.toString()

        // Convert value to a nbr and increment it:
        val cnt = incrementString(countString)
//        var count: Int = Integer.parseInt(countString)
//        count++

        // Display the new value in the text view:
//        textView.text = count.toString()
        textView.text = cnt
    }

    private fun incrementString(s: String, i: Int = 1): String {
        val result = try {
            val parsedInt = Integer.parseInt(s)
            val incrementedInt = parsedInt + i
            incrementedInt.toString()
        } catch (e: java.lang.NumberFormatException) {
            "42"
        }
        return result
    }
}

